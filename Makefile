main:
	g++ helloWorld.cpp -fPIC -lfastcgi-daemon2 -shared -o libhello.so
rundaemon:
	fastcgi-daemon2 --config=hello.conf
clean:
	rm libhello.so
